import enum
import numpy as np


from color_utils import polar


class DoughTypes(enum.IntEnum):
    classic = 0
    cheesy = 1
    thin = 2


def mad(vector, median=None):
    """MAD - median absolute deviation"""
    if median is None:
        median = np.median(vector)
    abs_deviation = np.abs(vector - median)
    return np.median(abs_deviation)


def _raw_features(crust_mask, center, num_sectors):
    sectors, Z = polar.divide_image(crust_mask, num_sectors, center=center)
    median_r = np.zeros(num_sectors)
    mad_r = np.zeros(num_sectors)
    for ind in range(num_sectors):
        mask = sectors == ind + 1
        z = Z[crust_mask & mask]
        r = np.abs(z)
        _median_r = np.median(r)
        median_r[ind] = _median_r
        mad_r[ind] = mad(r, median=_median_r)
    return median_r, mad_r


def compute_features(crust_mask, center, num_sectors, area):
    median_r, mad_r = _raw_features(crust_mask, center, num_sectors)
    scaled_mad_r = mad_r / median_r
    scaled_mad_r[np.isnan(scaled_mad_r)] = 0.0
    median_r[np.isnan(median_r)] = 0.0
    return median_r/np.sqrt(area), scaled_mad_r
