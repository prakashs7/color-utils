import json

from pizza_colors import PizzaClasses


class EmptyJsonError(Exception):
    pass


class InvalidJsonError(Exception):
    pass


def read_soft_annotation(soft_annotation_path, as_labels=False):
    """

    Args:
        soft_annotation_path (str): path to a soft annotation json file
        as_labels (bool): flag to decide if to return the toppings as labels

    Returns:
        list: the toppings as names or as labels

    Raises:
        EmptyJsonError, InvalidJsonError
    """
    with open(soft_annotation_path, 'r') as f:
        content = json.load(f)

    if not bool(content):
        raise EmptyJsonError("Json file %s is empty" % soft_annotation_path)

    if 'annotation' not in content:
        raise InvalidJsonError("Json file %s doesn't have 'annotation' key"
                               % soft_annotation_path)

    return _pasre_soft_annotation(content, as_labels=as_labels)


def _pasre_soft_annotation(content, as_labels):
    """

    Args:
        content (dict): content of soft annotation
        as_labels (bool): flag to decide if to return the toppings as labels

    Returns:
        list: the toppings as names or as labels
    """

    annotation = content['annotation']
    if isinstance(annotation, list):
        toppings = annotation[0].split(',')
    elif isinstance(annotation, str) or isinstance(annotation, unicode):
        toppings = annotation.split(',')
    toppings = [topping.strip() for topping in toppings if topping]
    if as_labels:
        return PizzaClasses.default().select('name', toppings).labels
    return toppings
