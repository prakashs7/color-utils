import os

import pytest

from color_utils.annotation_parser import read_soft_annotation
from color_utils.annotation_parser import EmptyJsonError, InvalidJsonError
from color_utils.pizza_colors import PizzaClasses


LOCAL_PATH = os.path.dirname(__file__)


def test_read_soft_annotation():

    pizza_classes = PizzaClasses.default()

    file_path = os.path.join(LOCAL_PATH, 'data', 'normal_5_toppings.json')
    expected_toppings = ['Beef', 'Italian Sausage', 'Pepperoni', 'Bacon',
                         'Ham']
    toppings = read_soft_annotation(file_path)
    assert toppings == expected_toppings

    file_path = os.path.join(LOCAL_PATH, 'data', 'missing_brackets.json')
    expected_toppings = ['Pineapple', 'Ham']
    toppings = read_soft_annotation(file_path)
    assert toppings == expected_toppings

    toppings_labels = read_soft_annotation(file_path, as_labels=True)
    expected_labels = pizza_classes.select('name', expected_toppings).labels
    assert toppings_labels == expected_labels

    # empty json
    file_path = os.path.join(LOCAL_PATH, 'data', 'empty.json')
    with pytest.raises(EmptyJsonError):
        read_soft_annotation(file_path)

    # missing annotation key in json
    file_path = os.path.join(LOCAL_PATH, 'data', 'missing_annotation_key.json')
    with pytest.raises(InvalidJsonError):
        read_soft_annotation(file_path)

    # empty list
    file_path = os.path.join(LOCAL_PATH, 'data', 'no_toppings.json')
    expected_toppings = []
    toppings = read_soft_annotation(file_path)
    assert toppings == expected_toppings
