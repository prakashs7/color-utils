import numpy as np

from color_utils.decider_features import compute_features
from color_utils.decider_features import get_pizza_area, relative_c_hull_area
from color_utils.decider_features import _compute_top_2_probabilities_diff
from color_utils.pizza_colors import PizzaClasses


CLASSES = PizzaClasses.default().filter('name', 'Unknown')


H = 600
W = 600
cx = W/2
cy = H / 2
radius = 240
crust_width = 20
pep_radius = 10
crust_label = CLASSES.select('name', 'Crust').labels[0]
cheese_label = CLASSES.select('name', 'Regular Cheese').labels[0]
pepperoni_label = CLASSES.select('name', 'Pepperoni').labels[0]
beef_label = CLASSES.select('name', 'Beef').labels[0]
pep_positions = [(200, 200), (200, 400), (400, 400), (400, 200), (300, 300)]
pepperoni_prob = 0.7
beef_prob = 0.3


def get_labels():
    labels = np.zeros((H, W), int)

    X, Y = np.meshgrid(np.arange(W), np.arange(H))
    pizza_sel = (X - cx)**2 + (Y - cy)**2 <= radius**2
    labels[pizza_sel] = cheese_label
    non_crust_sel = (X - cx)**2 + (Y - cy)**2 <= (radius - crust_width)**2
    crust_sel = pizza_sel & ~non_crust_sel
    labels[crust_sel] = crust_label
    pep_sel = np.zeros((H, W), bool)
    for p in pep_positions:
        pep_sel = pep_sel | ((X - p[0])**2 + (Y - p[1])**2 <= pep_radius**2)

    labels[pep_sel] = pepperoni_label

    # probabilities
    probs = np.zeros((H, W, CLASSES.num_classes))
    # background
    probs[~pizza_sel, 0] = 0.98
    probs[~pizza_sel, crust_label] = 0.02
    # crust
    probs[labels == crust_label, crust_label] = 0.9
    probs[labels == crust_label, cheese_label] = 0.1
    # cheese
    probs[labels == cheese_label, cheese_label] = 1.0
    # pepperoni
    probs[labels == pepperoni_label, pepperoni_label] = pepperoni_prob
    probs[labels == pepperoni_label, beef_label] = beef_prob
    return probs, labels


def test_get_pizza_area():
    _, labels = get_labels()
    area = get_pizza_area(labels)
    expected_area = np.pi * radius**2
    assert (expected_area - area) <= 1e-3 * expected_area


def test_convex_hull_area():
    _, labels = get_labels()
    pizza_area = get_pizza_area(labels)
    L = 200 + 2*pep_radius
    expected_relative_c_hull_area = L**2 / pizza_area
    _relative_c_hull_area = relative_c_hull_area(labels ==
                                                 pepperoni_label, pizza_area)
    assert (expected_relative_c_hull_area - _relative_c_hull_area) <= 1e-2


def test_top_2_probabilities_diff():
    probs, labels = get_labels()
    prob_diff = _compute_top_2_probabilities_diff(probs)
    excpected_diff = pepperoni_prob - beef_prob
    assert (prob_diff[labels == pepperoni_label] == excpected_diff).all()


def test_compute_features():
    probabilities, labels = get_labels()
    features = compute_features(probabilities, labels)
    i_rows, i_cols = np.nonzero(features)
    # only pepperoni has nonzero values
    assert (i_rows == pepperoni_label).all()
    assert (i_cols == np.arange(7)).all()

    pizza_area = get_pizza_area(labels)
    c_hull_area = relative_c_hull_area(labels == pepperoni_label, pizza_area)

    mask = labels == pepperoni_label
    # mean and median are different when the distribution is skewed
    assert (features[pepperoni_label, 0] ==
            probabilities[mask, pepperoni_label].sum())

    assert (features[pepperoni_label, 1] ==
            np.median(probabilities[mask, pepperoni_label]))

    # 0.7 - 0.3 is not 0.4 :(
    expected_diff = pepperoni_prob - beef_prob
    assert abs(features[pepperoni_label, 2] - expected_diff) < 1e-5

    assert ((features[pepperoni_label, 3] -
            probabilities[..., pepperoni_label].sum() / pizza_area) < 1e-5)

    assert abs(features[pepperoni_label, 4] - c_hull_area) < 1e-5

    assert features[pepperoni_label, 5] == len(pep_positions)

    assert abs(features[pepperoni_label, 6] -
               (np.pi * pep_radius ** 2)/pizza_area) < 1e-3
