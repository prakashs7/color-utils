import numpy as np

from color_utils import polar


def test_complex_grid():
    mask = np.zeros((4, 8))
    height, width = mask.shape
    res = polar.complex_grid(mask)
    # real axis corresponds to x
    assert (np.real(res)[0, :] == (np.arange(width) - width//2)).all()
    # imaginary axis corresponds to y
    assert (np.imag(res)[:, 0] == (np.arange(height) - height//2)).all()

    mask = np.zeros((4, 7))
    height, width = mask.shape
    res = polar.complex_grid(mask)
    # real axis corresponds to x
    assert (np.real(res)[0, :] == (np.arange(width) - width//2)).all()
    # imaginary axis corresponds to y
    assert (np.imag(res)[:, 0] == (np.arange(height) - height//2)).all()

    y_c = 1
    x_c = 5
    res = polar.complex_grid(mask, center=[y_c, x_c])
    # real axis corresponds to x
    assert (np.real(res)[0, :] == (np.arange(width) - x_c)).all()
    # imaginary axis corresponds to y
    assert (np.imag(res)[:, 0] == (np.arange(height) - y_c)).all()


def test_divide_image():
    num_sectors = 8
    mask = np.zeros((5, 5), bool)
    sectors, _ = polar.divide_image(mask, num_sectors)
    expected_sectors = np.array([[1, 2, 2, 3, 3],
                                 [1, 1, 2, 3, 4],
                                 [8, 8, 4, 4, 4],
                                 [8, 7, 6, 5, 5],
                                 [7, 7, 6, 6, 5]])
    assert (sectors == expected_sectors).all()

    offset = np.pi/16
    sectors, _ = polar.divide_image(mask, num_sectors, offset=offset)
    expected_sectors = np.array([[2, 2, 3, 3, 4],
                                 [1, 2, 3, 4, 4],
                                 [1, 1, 4, 5, 5],
                                 [8, 8, 7, 6, 5],
                                 [8, 7, 7, 6, 6]])
    assert (sectors == expected_sectors).all()

    center = (1, 1)
    sectors, _ = polar.divide_image(mask, num_sectors, center=center)
    expected_sectors = np.array([[1, 2, 3, 4, 4],
                                 [8, 4, 4, 4, 4],
                                 [7, 6, 5, 5, 5],
                                 [7, 6, 6, 5, 5],
                                 [7, 6, 6, 6, 5]])
    assert (sectors == expected_sectors).all()
