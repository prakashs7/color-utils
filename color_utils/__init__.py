import os
import pkg_resources  # part of setuptools

import yaml

# enable subpackages
# https://packaging.python.org/guides/packaging-namespace-packages/
__path__ = __import__('pkgutil').extend_path(__path__, __name__)

version = pkg_resources.require("color_utils")[0].version

THIS_DIR = os.path.dirname(__file__)


def get_toppings_rgb():
    """
    Returns:
        list[dict]: list of toppings (name, rgb)
    """
    with open(os.path.join(THIS_DIR, 'toppings_RGB.yaml'), 'r') as yml:
        toppings_rgb = yaml.load(yml)
    return toppings_rgb
