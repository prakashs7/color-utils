# color_utils

This repo contains the infamous pizza_colors.py from `AnalysisService` and 
other miscellaneous scripts required for training, validation pre/post processing 
of images, such as a parser for soft annotations. 

All the functionality is bundled up in `PizzaClasses` object that is a 
wrapper around a list of `PizzaClass`. It also accommodates the 'Unknown' 
class.

Please look at the tests for example use cases.


